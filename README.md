# IPPMggtheme

A `ggplot2()` theme for the FORMAS IPPM 2021 project.


## Installation

Install with the remotes package. First install remotes it you do not have it:

`install.package('remotes')`

With that you can install the `IPPMggtheme` with: 

`remotes::install_gitlab('ScientiaFelis/ippmggtheme')`
